function JoinToDomain {
    [CmdletBinding()]
    Param(
        [parameter(Mandatory=$true)]
        [string]$DomainName,
        [parameter(Mandatory=$true)]
        [string]$Username,
        [parameter(Mandatory=$true)]
        [string]$Password,
        [parameter(Mandatory=$true)]
        [string]$Dept
    )

    try {
        if ($env:COMPUTERNAME -eq $env:USERDOMAIN) {
            Write-Host "Not in domain"
            $adminname = "$DomainName\$Username"
            $adminpassword = ConvertTo-SecureString -asPlainText -Force -String "$Password"
            Write-Host "$adminname , $password"
            $credential = New-Object System.Management.Automation.PSCredential($adminname,$adminpassword)
            Add-Computer -DomainName $DomainName -Credential $credential -OUPath $Dept -Force -Options JoinWithNewName,AccountCreate -PassThru -ErrorAction Stop
        } else {
            Write-Host "Already in domain"
        }
    }
    catch {
        Write-Host "Join to domain failed, please check C:\Windows\debug\NetSetup.log in provisioned VM"
        Write-Host "ERROR_Domain=$($_.Exception.Message)"
        exit 0
    }
}

# Hashtable to store OU paths based on OU names
$OUs = @{
    "OU1" = "OU=OU1,OU=calm,DC=hb,DC=local"
    "OU2" = "OU=OU2,OU=calm,DC=hb,DC=local"
    "OU3" = "OU=OU3,OU=calm,DC=hb,DC=local"
    "OU4" = "OU=OU4,OU=calm,DC=hb,DC=local"
}

# Get the OU based on the provided string
$OU = $OUs["@@{OU}@@"]
if (-not $OU) {
    Write-Host "Domain join not required."
    exit 0
}
Write-Host $OU
JoinToDomain -DomainName "@@{DOMAIN}@@" -Username "@@{DOMAIN_CRED.username}@@" -Password "@@{DOMAIN_CRED.secret}@@" -Dept $OU

# Wait for 30 seconds to allow the domain join process to complete before shutting down
Start-Sleep -Seconds 30

# Shutdown the VM
Write-Host "Shutting down the computer..."
Stop-Computer -Force
