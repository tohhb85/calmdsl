print "Provisioning related summary is sent and in failure scenarios Email is sent with error message"

PC_User = "@@{PC_CRED.username}@@"
PC_Password = "@@{PC_CRED.secret}@@"
PC_IP = "@@{PC_IP}@@"
VM = "@@{name}@@"
email = "@@{email_address}@@"


#--[variables]--
headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
base_url = "https://" + PC_IP + ":9440/api/nutanix/v3/"
#--[Functions]--
def setconfig(api_url, verb, payload):
  """
  Basic function to run a api call. Use this function once you have the endpoint, verb and payload(if needed)
  :param api_url: Endpoint
  :param verb: Method
  :param payload: body
  :return: payload
  """
  r = urlreq(api_url, verb=verb, auth="BASIC", user=PC_User, passwd=PC_Password, headers={'Content-type': 'application/json','Accept': 'application/json'}, params=json.dumps(payload), verify=False)
  if r.ok:
    resp = json.loads(r.content)
    return resp
  else:
    print "Rest api request failed", r.content
    exit(0)


def getVMList():
  """
  Function to get the name and UUID of VM.
  :param: none
  :return: list of UUID of the VM
  """
  url = base_url + "/vms/list"
  payload = {"kind": "vm","length": 250}
  payload = setconfig(api_url = url, verb = "POST", payload = payload)
  for data in payload['entities']:
    if data['status']['name']==VM:
        name=data['status']['name']
        uuid=data['metadata']['uuid']
  return uuid

def GETVM(uuid):
    url = base_url +"vms/"+uuid
    payload = {"kind": "vm","length": 250}
    payload = setconfig(api_url = url, verb = "GET", payload = payload)
    vmname = payload['spec']['name']
    ip_list=[]
    name=payload['status']['resources']['nic_list']
    for name1 in name:
        nic=name1['ip_endpoint_list']
        for ip in nic:
            ip_address=ip['ip']
            ip_list.append(ip_address)
    ipaddress = str(ip_list).replace('[','').replace(']','')
    project=payload['metadata']['project_reference']['name']
    owner=payload['metadata']['owner_reference']['name']
    cpu=payload['status']['resources']['num_vcpus_per_socket']
    cpu_usage=str(cpu)
    cores=payload['status']['resources']['num_sockets']
    core_usage=str(cores)
    memory=payload['status']['resources']['memory_size_mib']
    memory_usage=str(memory)
    data=payload['status']['resources']['disk_list']
    for storage1 in data:
        if storage1['device_properties']['device_type']=="DISK":
            disk=storage1['disk_size_mib']
            disk_usage=str(disk)
    out=payload['metadata']['categories_mapping']

    category_usage =str(out)
    category = str(category_usage).replace('{','').replace('}','')
    mail_string = ("The following details for VM %s deployed.\n Project Name: %s \n Project Owner: %s \n Cpu: %s \n Cores: %s \n Memory Allocated: %s MB \n Disk: %s MiB \n Ipaddress: %s \n Categories: %s "%(vmname,project,owner,cpu_usage,core_usage,memory_usage,disk_usage,ipaddress,category))
    print(mail_string)
    dict = { "hostname" : vmname, "project" : project, "owner" : owner , "cpu" : cpu_usage, "Cores" : core_usage, "memory": memory_usage, "disk" : disk_usage, "ipaddress": ipaddress, "category" : category}
    dict_json = json.dumps(dict)
    return dict_json,mail_string



def json2xml(json_obj, line_padding=""):
    result_list = list()
    json_obj_type = type(json_obj)
    if json_obj_type is list:
        for sub_elem in json_obj:
            result_list.append(json2xml(sub_elem, line_padding))
        return " ".join(result_list)

    if json_obj_type is dict:
        for tag_name in json_obj:
            sub_obj = json_obj[tag_name]
            result_list.append("%s<%s>" % (line_padding, tag_name))
            result_list.append(json2xml(sub_obj, " " + line_padding))
            result_list.append("%s</%s>" % (line_padding, tag_name))
        return " ".join(result_list)
    return "%s%s" % (line_padding, json_obj)


uuid=getVMList()

dict_json,mail_string = GETVM(uuid)

j = json.loads(dict_json)
xmlformat = json2xml(j)
header = '<?xml version = "1.0" encoding = "UTF-8"?>'+ ' '
result = header + xmlformat
print "OutputXML =" + result

def Send_Email(mail_string):
    url = base_url +'action_rules/trigger'
    payload = {"trigger_type": "incoming_webhook_trigger","trigger_instance_list": [{"webhook_id": "6aa94ea5-e0a7-4fa3-be64-82111db2dd25", "string1" : mail_string, "string2": email}]}
    out = setconfig(api_url = url, verb = "POST", payload = payload)
Send_Email(mail_string)