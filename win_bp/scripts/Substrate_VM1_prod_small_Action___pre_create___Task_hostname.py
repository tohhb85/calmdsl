PC_IP = "@@{PC_IP}@@"
PC_User = "@@{PC_CRED.username}@@"
PC_Password = "@@{PC_CRED.secret}@@"
hostname = ""
vm_name = "@@{basename}@@"

if not hostname:
  #Function for REST call
  headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
  base_url = "https://" + PC_IP + ":9440/api/nutanix/v3/"

  #--[Functions]--
  def setconfig(api_url, verb, payload):
    """
    Basic function to run a api call. Use this function once you have the endpoint, verb and payload(if needed)
    :param api_url: Endpoint 
    :param verb: Method
    :param payload: body
    :return: payload
    """
    r = urlreq(api_url, verb=verb, auth="BASIC", user=PC_User, passwd=PC_Password, headers={'Content-type': 'application/json','Accept': 'application/json'}, params=json.dumps(payload), verify=False)
    if r.ok:
      resp = json.loads(r.content)
      return resp
    else:
      print "Rest api request failed", r.content
      exit(1)
      

  def Get_VMs():
    url= base_url + 'vms/list'
    payload= { "kind": "vm", "filter": "vm_name=="+vm_name+".[0-9]*$", "length": 250 }
    payload = setconfig(api_url=url, verb='POST', payload=payload)
    return payload['entities']
    
    
    
  vmlist=[]
  out = Get_VMs()

  for i in out:
    vmlist.append(i['status']['name'])

  #basename = [i for i in vmlist if i.upper().startswith(vm_name.upper())] #Only needed if filter in api call is not apt. if using chnage below veriables
  vmlist = sorted(vmlist, reverse=True)
  #vmlist = ['hb-win0010', 'hb-win0004', 'hb-win0003', 'hb-win0002', 'hb-win0001']
  print vmlist

def generate_next_hostname(vm_list, vm_prefix):
    # Filter the list to include only VM names starting with the given prefix
    filtered_list = [name for name in vm_list if name.startswith(vm_prefix)]
    if not filtered_list:
        # If no VMs with the given prefix are found, assign the first index as "0001"
        next_num = '0001'
    else:
        # Find the first available gap in the sequence of numbers
        max_index = max(int(name.split(vm_prefix)[1]) for name in filtered_list)
        all_indices = set(range(1, max_index + 1))
        used_indices = set(int(name.split(vm_prefix)[1]) for name in filtered_list)
        available_indices = all_indices - used_indices

        if available_indices:
            # If there are available indices, use the lowest one
            next_num = str(min(available_indices)).zfill(4)
        else:
            # If there are no available indices, increment the maximum index
            next_num = str(max_index + 1).zfill(4)
    
    return vm_prefix + next_num

hostname = generate_next_hostname(vmlist, vm_name)
print "HOSTNAME="+hostname
