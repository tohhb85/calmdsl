$serverPath = "@@{fileshare_path}@@"
$filenames = "@@{file_name_1}@@",
            "@@{file_name_2}@@",
            "@@{file_name_3}@@"
            
Get-WSManCredSSP
try {
    # Ensure all files exist before proceeding with the installation
    $files = $filenames | ForEach-Object { Join-Path $serverPath $_ }
    $missingFiles = $files | Where-Object { -Not (Test-Path $_ -PathType Leaf) }

    if ($missingFiles.Count -gt 0) {
        $missingFiles | ForEach-Object {
            Write-Warning "File not found: $_"
        }
        Exit
    }

    Write-Host "All installers found in share drive, starting installation."

    $tempDir = "C:\Agent_Temp"
    $null = New-Item -ItemType Directory -Path $tempDir -Force
    $null = $files | Copy-Item -Destination $tempDir -Force

    $localFiles = Get-ChildItem (Join-Path $tempDir "*.msi")

    Write-Host "Installing tools..."
    foreach ($localFile in $localFiles) {
        $process = Start-Process -FilePath $localFile -ArgumentList "/qn" -Wait -PassThru
    }

    Write-Host "Done, cleaning up installation folder"
    Remove-Item -Path $tempDir -Force -Recurse -ErrorAction SilentlyContinue
    Disable-WSManCredSSP -Role Server
    Get-WSManCredSSP

}
catch {
    Write-Host "An error occurred:"
    Write-Host $_
    Exit  # Stop PowerShell!
}
