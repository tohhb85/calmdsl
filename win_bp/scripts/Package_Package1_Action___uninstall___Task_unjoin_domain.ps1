#unjoin domain
try{
	$adminname = "@@{DOMAIN_CRED.username}@@"
    $adminpassword = ConvertTo-SecureString -asPlainText -Force -String "@@{DOMAIN_CRED.secret}@@"
    $credential = New-Object System.Management.Automation.PSCredential($adminname,$adminpassword)
    Remove-Computer -Credential $credential -Force
   }
catch{
	$Error= "Unjoin domain failed"
    exit 1
     }
    